<?php

class Badge{
	
	public $id;
	public $userId;
	public $name;
	public $date;
	
	function __construct($idIn, $userIdIn, $nameIn, $dateIn){
		$this -> id = $idIn;
		$this -> userId = $userIdIn;
		$this -> name = $nameIn;
		$this -> date = $dateIn;
	}
	
	public function getId(){
		return $this -> id;
	}

	public function getUserId(){
		return $this -> userId;
	}
	
	public function getName(){
		return $this -> name;
	}

	public function getDate(){
		return $this -> date;
	}
	
	public function echoBadge(){
		echo "Id = ", $this -> id," User Id = ", $this -> userId," Name = ", $this -> name," Date = ",
		$this -> date, "<br>";
	}
	
}


function BadgeParser(){
	$xml = simplexml_load_file("Badges.xml");
    $badges = array();
	foreach($xml->row as $a){
		$tempId;
		$tempUserId;
		$tempName;
		$tempDate;
		foreach($a->attributes() as $b => $c) {
			if($b == "Id"){
				$tempId = $c;
			}
		    elseif($b == "UserId"){
				$tempUserId = $c;
			}
		    elseif($b == "Name"){
				$tempName = $c;
			}
		    elseif($b == "Date"){
				$tempDate = $c;
			}
		}
		$badge = new Badge($tempId, $tempUserId, $tempName, $tempDate);
		array_push($badges, $badge);
	}
	return $badges;
}

$badgeArray = BadgeParser();

?>