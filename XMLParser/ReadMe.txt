In order to populate the 'temporaltrust' database
the following operations should be performed.

1	Run the DatabaseBridge.php file in the Bicycles
	folder. This creates the database and adds bicycle
	data.
	
2	Run both the DatabaseBridge.php file from the the 
	HomeImprovement and Photography folders. The order
	these are done in makes no difference.