<?php

class Comment{
	
	public $id;
	public $postId;
	public $text;
	public $creationDate;
	public $userId;
	
	function __construct($idIn, $postIdIn, $textIn, $creationDateIn, $userIdIn){
		$this -> id = $idIn;
		$this -> postId = $postIdIn;
		$this -> text = $textIn;
		$this -> creationDate = $creationDateIn;
		$this -> userId = $userIdIn;
	}
	
	public function getId(){
		return $this -> id;
	}

	public function getPostId(){
		return $this -> postId;
	}
	
	public function getText(){
		return $this -> text;
	}

	public function getCreationDate(){
		return $this -> creationDate;
	}
	
    public function getUserId(){
		return $this -> userId;
	}
	
	public function echoComment(){
		echo "Id = ", $this -> getId()," Post Id = ", $this -> getPostId()," Text = ", $this -> getText()," Creation Date = ",
		$this -> getCreationDate(), " User id = ", $this -> getUserId(), "<br>";
	}
	
}


function CommentParser(){
	$xml = simplexml_load_file("Comments.xml");
    $Comments = array();
	foreach($xml->row as $a){
		$tempId;
		$tempPostId;
		$tempText;
		$tempCreationDate;
		$tempUserId;
		foreach($a->attributes() as $b => $c) {
			if($b == "Id"){
				$tempId = $c;
			}
		    elseif($b == "PostId"){
				$tempPostId = $c;
			}
		    elseif($b == "Text"){
				$tempText = $c;
			}
		    elseif($b == "CreationDate"){
				$tempCreationDate = $c;
			}
		    elseif($b == "UserId"){
				$tempUserId = $c;
			}
		}
		$Comment = new Comment($tempId, $tempPostId, $tempText, $tempCreationDate, $tempUserId);
		array_push($Comments, $Comment);
	}
	return $Comments;
}

$CommentArray = CommentParser();

/*foreach($CommentArray as $u){

   echo $u->creationDate;

}*/

?>