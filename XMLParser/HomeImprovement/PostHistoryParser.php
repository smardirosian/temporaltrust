<?php

class PostHistory{
	
	public $id;
	public $postHistoryTypeId;
	public $postId;
	public $revisionGUID;
	public $creationDate;
	public $userId;
	public $text;
	
	function __construct($idIn, $postHistoryTypeIdIn, $postIdIn,
	$revisionGUIDIn, $creationDateIn, $userIdIn, $textIn){
		$this -> id = $idIn;
		$this -> postHistoryTypeId = $postHistoryTypeIdIn;
		$this -> postId = $postIdIn;
		$this -> revisionGUID = $revisionGUIDIn;
		$this -> creationDate = $creationDateIn;
		$this -> userId = $userIdIn;
		$this -> text = $textIn;
	}
	
	public function getId(){
		return $this -> id;
	}

	public function getPostHistoryTypeId(){
		return $this -> postHistoryTypeId;
	}	
	
    public function getPostId(){
		return $this -> postId;
	}
	
	public function getRevisionGUID(){
		return $this -> revisionGUID;
	}
		
	public function getCreationDate(){
		return $this -> creationDate;
	}

    public function getUserId(){
		return $this -> userId;
	}	
	
	public function getText(){
		return $this -> text;
	}

	
	public function echoPostHistory(){
		echo "Id = ", $this -> getId()," Post History Type Id = ", $this -> getPostHistoryTypeId()," Post Id = ", $this -> getPostId(),
		" Post Revision GUID = ", $this -> getRevisionGUID()," Creation Date = ", $this -> getCreationDate(), " User id = ", 
		$this -> getUserId(), " Text = ", $this -> getText(), "<br>";
	}
	
}


function PostHistoryParser(){
	$xml = simplexml_load_file("PostHistory.xml");
    $PostHistories = array();
	foreach($xml->row as $a){
		$tempId;
		$tempPostHistoryTypeId;
		$tempPostId;
		$tempRevisionGUID;
		$tempCreationDate;
		$tempUserId;
		$tempText;
		foreach($a->attributes() as $b => $c) {
			if($b == "Id"){
				$tempId = $c;
			}
			elseif($b == "PostHistoryTypeId"){
				$tempPostHistoryTypeId = $c;
			}
		    elseif($b == "PostId"){
				$tempPostId = $c;
			}
			elseif($b == "RevisionGUID"){
				$tempRevisionGUID = $c;
			}
			elseif($b == "CreationDate"){
				$tempCreationDate = $c;
			}
		    elseif($b == "UserId"){
				$tempUserId = $c;
			}
		    elseif($b == "Text"){
				$tempText = $c;
			}
		}
		$PostHistory = new PostHistory($tempId, $tempPostHistoryTypeId, $tempPostId,$tempRevisionGUID, $tempCreationDate, $tempUserId, $tempText);
		array_push($PostHistories, $PostHistory);
	}
	return $PostHistories;
}

$PostHistoryArray = PostHistoryParser();
 
?>