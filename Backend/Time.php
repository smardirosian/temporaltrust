<?php

class Time
{
	public static $lowerbounddate = "2010-1-1";

	static function SubTime($t_one, $t_two)
	{
		if(gettype($t_one) == "string"){
			$t_one = self::GetAsIntSeconds($t_one);
		}
		if(gettype($t_two) == "string"){
			$t_two = self::GetAsIntSeconds($t_two);
		}
		return ($t_one - $t_two);
	} 




	static function GetAsIntSeconds($time)
	{
		$temp_t = split("[:.T]", $time);
		$thisdate = new DateTime($temp_t[0]);

		$earliestdate = new DateTime(self::$lowerbounddate);

		$temp = date_diff($thisdate, $earliestdate);
		$total = $temp->days;

		$total = ($total * 24) + intval($temp_t[1]);
		$total = ($total * 60) + intval($temp_t[2]);
		$total = ($total * 60) + intval($temp_t[3]);
		return $total;
	}

	static function GetAsDate($time)
	{

	}

	static function TimeCompare($t_one, $t_two)
	{
		$diff = self::SubTime($t_one, $t_two);
		if($diff < 0)
		{
			return -1;
		}
		else if($diff == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}


?>