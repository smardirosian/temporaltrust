<?php
ini_set('max_execution_time', 0);
include "Time.php";
include "System.php";

class BaseFunction
{
	public $system;

	function __construct($dataset)
	{
		$this->system = new LTTM_L($dataset);
	}

	function Cardinality($input_set)
	{
		return count($input_set);
		foreach($input_set as $temp)
			echo $temp;
	}

	function TimeFunction($tuple)
	{
		return $tuple[1];
	}

	function SourceFunction($tuple)
	{
		return $tuple[0];
	}

	function TargetFunction($tuple)
	{
		return $tuple[2];
	}

	function LifeCycleFunction($v)
	{
		$t_0 = $this->system->time_0;
		$diff = Time::SubTime($v,$t_0);

		if($diff < 0)
		{
			return 0;
		}
		return $diff;
	}

	function AgentBornFunction($agentid)
	{
		foreach($this->system->agent_set as $agent)
		{
			if($agent["EntityId"] == $agentid)
			{
				return $agent["Time"];
			}
		}
	}

	function AgentLifeCycleFunction($agentid, $v)
	{
		foreach($this->system->agent_set as $agent)
		{
			if($agent["EntityId"] == $agentid)
			{
				$diff = Time::SubTime($v, $agent["Time"]);
				if($diff < 0){
					return 0;
				}
				return $diff;
			}
		}
	}

	function SystemInteractionsFunction($v)
	{
		return $this->system->interactions_table;
	}


	function IntervalAgentInputInteractionsFunction($y, $t_lower, $t_upper)
	{
		$set = array();
		$index = 0;
		foreach($this->system->interactions_table as $interaction)
		{
			if($interaction["BetaId"] == $y)
			{
				if((Time::TimeCompare($interaction["Time"], $t_lower) >= 0) && 
					(Time::TimeCompare($interaction["Time"], $t_upper) <= 0) )
				{
					$set[$index++] = $interaction;
				}
			}
		}
		return $set;
	}

	function IntervalAgentOutputInteractionsFunction($y, $t_lower, $t_upper)
	{
		$set = array();
		$index = 0;
		foreach($this->system->interactions_table as $interaction)
		{
			if($interaction["AlphaId"] == $y)
			{
				if((Time::TimeCompare($interaction["Time"], $t_lower) >= 0) && 
					(Time::TimeCompare($interaction["Time"], $t_upper) <= 0) )
				{			
					$set[$index++] = $interaction;
				}
			}
		}
		return $set;
	}

	function PresenceFunction($y, $t_lower, $t_upper)
	{
		$m = $this->Cardinality($this->IntervalAgentOutputInteractionsFunction($y, $t_lower, $t_upper)) 
			+ $this->Cardinality($this->IntervalAgentInputInteractionsFunction($y, $t_lower, $t_upper));
		if($m > 0)
		{
			return 1;
		}
		else return 0;
	}

}

?>