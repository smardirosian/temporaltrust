function OnChange(value)
		{
       		$.getJSON( "FetchInteraction.php", 'val=' + value, function( data ) {}); 
			$.getJSON( "FetchEntity.php", 'val=' + value, function( data ) {});
			function log( message ) {
				$( "<div>" ).text( message ).prependTo( "#log" );
				$( "#log" ).scrollTop( 0 );
			}
			jQuery(document).ready(function($){
			$('#users').autocomplete({source:'search.php?val='+ value, minLength:2, 
				select: function( event, ui ) {
       			 log( ui.item ?
          		"Selected: " + ui.item.value + " ID " + ui.item.id :
          		"Nothing selected, input was " + this.value );
      			}});
			});
			drawGraphs(value);
    		return true;   	
    	}