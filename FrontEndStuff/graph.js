
	var dataArray = [20, 30, 40, 50, 60];
	var textArray = ["bar 1", "bar 2", "bar 3", "bar 4", "bar 5"];

	var width = 600;
	var height = 600;

	var widthScale = 	d3.scale.linear()
						.domain([0, d3.max(dataArray)])
						.range([0, width]);

	var axisX = 	d3.svg.axis()
						.ticks(5)
						.scale(widthScale);

	var canvas = 	d3.select("body")
					.append("svg")
					.attr("width", width)
					.attr("height", height)
					.style("padding", 50)

	var bars = 	canvas.selectAll("rect")
				.data(dataArray)
				.text(textArray)
				.enter()
					.append("rect")
					.attr("width", function(d){return widthScale(d)})
					.attr("height", 50)
					.attr("fill", "steelblue") 
					.attr("y", function(d, i) {return i * 100;})

	canvas.append("g")
		.attr("transform", "translate(0, 500)")
		.call(axisX);